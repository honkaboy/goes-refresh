#!/usr/local/bin/python

import mechanize
import cookielib
from BeautifulSoup import BeautifulSoup
import html2text
import credentials

# debug
import re

# Browser
br = mechanize.Browser()

# Cookie Jar
cj = cookielib.LWPCookieJar()
br.set_cookiejar(cj)

# Browser options
br.set_handle_equiv(True)
br.set_handle_gzip(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)
br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

br.addheaders = [('User-agent', 'Chrome')]

# The site we will navigate into, handling it's session
br.open('https://goes-app.cbp.dhs.gov/goes/');

# Select the second (index one) form (the first form is a search query box)
br.select_form(nr=1)

# User credentials
br.form['j_username'] = credentials.username
br.form['j_password'] = credentials.password

for form in br.forms():
    print "Form name:", form.name
    print form

#for link in br.links():
#    #match = re.search('manageAptm', str(link))
#    print link 

#for control in br.form.controls:
#    print control
#    print "type=%s, name=%s value=%s" % (control.type, control.name, br[control.name]) 

# Login
br.submit()

enter = br.find_link(url='/goes/HomePagePreAction.do')
response = br.follow_link(enter)

br.select_form('ApplicationActionForm')
manageAptm = br.form.find_control('manageAptm')
manageAptm.click()

#print(br.open('https://github.com/settings/emails').read())
