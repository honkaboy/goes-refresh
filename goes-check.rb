#!/usr/bin/env ruby
require 'watir-webdriver'
require 'yaml'

username = begin
  YAML.load_file('credentials.yml').fetch('username')
rescue Errno::ENOENT, KeyError
  '.'
end

password = begin
  YAML.load_file('credentials.yml').fetch('password')
rescue Errno::ENOENT, KeyError
  '.'
end

b = Watir::Browser.new :chrome
b.goto 'https://goes-app.cbp.dhs.gov/goes'

# Log in
t = b.text_field(:id => 'j_username').set username
t = b.text_field(:id => 'j_password').set password 
b.button(:name => 'Sign In').click

# Get to appointment calendar
b.link(:text => 'Enter').click
b.button(:text => 'Manage Interview Appointment').click
b.button(:text => 'Reschedule Appointment').click
b.select_list(:name => 'selectedEnrollmentCenter').select('San Francisco Global Entry Enrollment Center - San Francisco International Airport, San Francisco, CA 94128, US')
b.button(:text => 'Next').click
